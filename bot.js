// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const { ActivityTypes } = require('botbuilder');
const { ActivityHandler } = require('botbuilder'); 

// Flag needed to display welcome message once and not twice
// var flag = 1;
// Setting confidence score here
var confidenceScore = 0.57;

class MyBot extends ActivityHandler {
    /**
     *
     * @param {TurnContext} on turn context object.
     */

    constructor(qnaServices) {
        super();
        this.qnaServices = qnaServices;

        // WHEN MESSAGE FROM USER DETECTED, GIVE AN APPROPRIATE REPLY
        this.onMessage(async (context, next) => {
            for (let i = 0; i < this.qnaServices.length; i++) {
                const qnaResults = await this.qnaServices[i].getAnswers(context);
                //If we have a recieved a reply in our buffer and the confidenceScore is above the treshold => give reply
                if(qnaResults[0] && qnaResults[0].score > confidenceScore) {
                    await context.sendActivity(qnaResults[0].answer);
                    return;
                }
                else {
                    await context.sendActivity("No result found. Try phasing the question differently or simply use keywords");
                }
            }
            // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        });
        
        // IF NEW USER CONNECTING, GIVE WELCOME MESSAGE
        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await context.sendActivity('Welcome to Gards chatbot. I can help you with problems regarding the bill of lading. Ask me a question or give me keywords about what you want to know more about, and I will give you the appropriate information.');
                }
            }
            await next();
        });
    }

/*
    async onTurn(turnContext) {
        // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        //If message recieved => check it, if no message recieved, a new user has connected
        if (turnContext.activity.type == ActivityTypes.Message) {
            for (let i = 0; i < this.qnaServices.length; i++) {
                const qnaResults = await this.qnaServices[i].getAnswers(turnContext);
                //If we have a recieved a reply in our buffer and the confidenceScore is above the treshold => give reply
                if(qnaResults[0] && qnaResults[0].score > confidenceScore) {
                    await turnContext.sendActivity(qnaResults[0].answer);
                    return;
                }
                else {
                    await turnContext.sendActivity("No result found. Try phasing the question differently or simply use keywords");
                }
            }
            await turnContext('No answers were found');

            //Welcome message
        } else {
            flag++;
            if(flag % 2 == 0){
                //await turnContext.sendActivity(`[${ turnContext.activity.type } event detected]`);
                await turnContext.sendActivity('Welcome to Gards chatbot. I can help you with problems regarding the bill of lading. Ask me a question or give me keywords about what you want to know more about, and I will give you the appropriate information.');
            }
        }
    }
    */
}



module.exports.MyBot = MyBot;
